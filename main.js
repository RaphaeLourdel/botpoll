const SlackBot = require('slackbots');
var express = require('express');
var request = require('request');
var bodyParser = require('body-parser');
var app = express();
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var timestamp;
var tabRestaurant; //Stockage des restaurants avec users
var TOKENBOT = process.env.SLACK_TOKEN;
var type = []; //Récupération éléments de /vote
var channelName;
var channelID;
var tabMultiPoll = []; //Stocker les différents polls

const bot = new SlackBot({
    token: TOKENBOT,
    name : 'sondage',
});

var listener = app.listen(4440, function() {
	console.log(listener.address().port);
})

app.post('/vote', urlencodedParser, (req, res) => {
    res.status(200).end();
    var reqBody = req.body; //Récupération de la reqûete
    tabRestaurant = []; //Initialisation d'un tableau de restaurants
    getMessage(reqBody.text); //Récupération param en entrée de /vote et initilisation du type de restaurant
    addRestaurant(); //Construction du tabRestaurant
    var voteBouton = [];
    channelName = reqBody.channel_name;
    
    //construction du formulaire
    for(var i = 0; i < type.length; i++) {
        var tmp = {"name":type[i], "text":type[i], "type":"button", "value":type[i]};
        voteBouton.push(tmp);
    }

    if(type.length == 1){

        bot.postMessageToChannel(channelName, 'Veuillez entrer des propositons', { attachments: attach}, (res, err) => {
        });

    }else if(type.length > 5){

        bot.postMessageToChannel(channelName, 'Veuillez entrer 5 propositions maximum', { attachments: attach}, (res, err) => {
        });

    }else{

        var attach = [
        {
            "callback_id": "tender_button",
            "fallback" : "vote",
            "actions": voteBouton
        }
        ];

    //Envoie de la requête POST pour le formulaire*/
    bot.postMessageToChannel(channelName, 'Cliquer sur un bouton pour voter', { attachments: attach}, (res, err) => {
        timestamp = res.ts;
        addTimeStamp(timestamp);
        var poll = new Poll(tabRestaurant);
        tabMultiPoll.push(poll);
    });
}
});

app.post('/action', urlencodedParser, (req, res) => {

    res.status(200).end();
    var actionJSONPayload = JSON.parse(req.body.payload) // parse URL-encoded payload JSON string
    //recherche restaurant et ajout utilisateur
    channelID = actionJSONPayload.channel.id;
    findRestaurant(actionJSONPayload.message_ts, actionJSONPayload.user.id, actionJSONPayload.actions[0].name);
});



function updateMessage(message, ts) {

	var path = 'https://slack.com/api/chat.update?token='+TOKENBOT+'&channel='+channelID+'&text='+message+'&ts='+ts;

	request(path, (error, response, body) => {
		if(!error && response.statusCode == 200) {
			console.log('Success');
		} else {
			console.log(error);
		}
	})
};

//Constructeur Restaurant
function Restaurant(name, tab, timestamp) {

    this.name = name;
    this.tab = tab;
    this.timestamp = timestamp;
}

//Constructeur Poll
function Poll(tab) {
    
    this.tab = tab;
}

//Ajout des restaurants au tableau
function addRestaurant() {

    for(var i = 0; i < type.length; i++){
      var restaurant = new Restaurant(type[i],[],0);
      tabRestaurant.push(restaurant);
  }
}

//Récupération des restaurants passés en entrée
function getMessage(message) {

    type = message.split(',');
}

function addTimeStamp(ts) {

    for(var i = 0; i < tabRestaurant.length; i++) {
        tabRestaurant[i].timestamp = ts;
    }
}

//Recherche restaurant et add/remove user
function findRestaurant(ts, user, nameRestaurant) {

    var tmp = 0; //Sauvegarder le poll concerné par l'action
    var time = ts; //Sauvegarde du timeStamp pour updateMessage()
    //Pour chaque poll
    for(var i = 0; i < tabMultiPoll.length; i++) {
        //Taille du poll
        for(var j = 0; j < tabMultiPoll[i].tab.length; j++) {
            //Si meme timestamp que l'action
            if(tabMultiPoll[i].tab[j].timestamp == ts) {
                //Si meme non que l'action
                if(tabMultiPoll[i].tab[j].name == nameRestaurant) {

                    var userExist = false;
                    //Recherche de l'user de l'action
                    for(var x = 0; x < tabMultiPoll[i].tab[j].tab.length; x++) {
                        //Si user trouve
                        if(tabMultiPoll[i].tab[j].tab[x] == "<@"+user+">") {
                            //Suppression de l'utilisateur du vote
                            userExist = true;
                            tabMultiPoll[i].tab[j].tab.splice(x,1);
                            tmp = i;
                            break
                        }
                    }//Si user pas trouve
                    if(userExist == false) {
                        tabMultiPoll[i].tab[j].tab.push("<@"+user+">");
                        tmp = i;
                        break;
                    }
                }
            }
        }
   }
   constructMessage(tmp, time);
}

function constructMessage(tmp, time) {
    //Initialisation du message final
    var textFinal = "";
    var ts = time; //Sauvegarde du timeStamp pour updateMessage()

    for(var i = 0; i < tabMultiPoll[tmp].tab.length; i++) {

        var textUser = "";

        //Initialisation des users au restaurant d'indice i
        for(var j = 0; j < tabMultiPoll[tmp].tab[i].tab.length; j++) {
            //Ajout des users
            textUser = textUser+" "+tabMultiPoll[tmp].tab[i].tab[j];
        }
        //Concat name restaurant + users
        textUser = tabMultiPoll[tmp].tab[i].name+" = "+tabMultiPoll[tmp].tab[i].tab.length+" : "+textUser+"\n";
        textFinal = textFinal+textUser;
    }
    
    updateMessage(textFinal, ts);
}